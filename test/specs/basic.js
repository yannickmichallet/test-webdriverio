const assert = require('assert')
const fs = require('fs')
const csv = require('fast-csv')

describe('Google Search page', () => {
    it('should use the csv data to search in Webdriver.IO API docs', () => {
            
        let rows = [];

        fs.createReadStream('test/specs/my.csv')
            .pipe(csv.parse({ headers: true, rowDelimiter: ';' }))
            .on('data', row => rows.push(row));

        browser.pause(1000);

        rows.forEach(row => {

            let info1 = row.header1;
            let info2 = row.header2;

            browser.url('https://www.google.fr')
            const title = browser.getTitle()
            assert.strictEqual(title, 'Google')
            
            // On recherche un mot
            const elem = $('input[title="Rechercher"]')
            elem.setValue('WebdriverIO')
            // On click sur le bouton "Rechercher"
            const buttonRechercher = $('input[aria-label="Recherche Google"]');
            buttonRechercher.click();
            // On click sur le 1er résultat de la liste
            $("#search .rc .r a").click()
            assert.ok(browser.getTitle().includes('WebdriverIO')); // VERIFICATION
            
            // Click sur le lien "API"
            $('a[href="/docs/api.html"]').click();
            $('#search_input_react').setValue(info1 + " - " + info2);
            
            //browser.pause(1000);
            // Envoie de touches ...
            //browser.keys(['\uE015' /* down */, '\uE013' /*up*/]);
            //browser.pause(5000);
        });
            
    })
})
